package e.wiantoro.myapplication;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import e.wiantoro.myapplication.api.ApiConnection;
import e.wiantoro.myapplication.model.Artikel;
import e.wiantoro.myapplication.response.ArtikelResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements ArtikelAdapter.OnClickCallback{

    private RecyclerView rvItems;

    private ProgressBar progressBar;

    private ArtikelAdapter adapter;


    private final String KEY_RECYCLER_STATE = "recycler_state";

    private static Bundle bundle;

    private Parcelable mListState = null;

    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvItems = findViewById(R.id.rv_items);

        progressBar = findViewById(R.id.progressbar);


        linearLayoutManager = new LinearLayoutManager(this);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        else linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);


        rvItems.setHasFixedSize(true);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        adapter = new ArtikelAdapter();
        adapter.setOnClickCallback(this);
        adapter.setArtikels(new ArrayList<Artikel>());

        rvItems.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);

        if (adapter.getArtikels().isEmpty()) getItem();
    }

    private void  getItem(){
        ApiConnection.getTeamList("wsj.com", BuildConfig.Api)
                .enqueue(new Callback<ArtikelResponse>() {
                    @Override
                    public void onResponse(Call<ArtikelResponse> call,
                                           Response<ArtikelResponse> response) {

                        progressBar.setVisibility(View.GONE);

                        adapter.setArtikels(response
                                .body().getArtikels());
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<ArtikelResponse> call,
                                          Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        toast("gagal tidak terhubung ke server");
                    }
                });
    }

    private void toast(String message){
        Toast.makeText(this, message,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClicked(Artikel artikel) {
        Intent intent = new Intent(MainActivity.this,
                DetailArtikelActivity.class);
        intent.putExtra(DetailArtikelActivity.BUNDLE_GAMBAR,
                artikel.getUrlToImage());
        intent.putExtra(DetailArtikelActivity.BUNDLE_TITLE,
                artikel.getTitle());
        intent.putExtra(DetailArtikelActivity.BUNDLE_DESCRIPTION,
                artikel.getDescription());
        intent.putExtra(DetailArtikelActivity.BUNDLE_AUTHOR,
                artikel.getAuthor());
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (bundle != null) {
            mListState = bundle.getParcelable(KEY_RECYCLER_STATE);
            rvItems.getLayoutManager().onRestoreInstanceState(mListState);
        }

        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rvItems.setLayoutManager(linearLayoutManager);
        } else if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvItems.setLayoutManager(linearLayoutManager);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        bundle = new Bundle();
        mListState = rvItems.getLayoutManager().onSaveInstanceState();
        bundle.putParcelable(KEY_RECYCLER_STATE, mListState);
    }


}
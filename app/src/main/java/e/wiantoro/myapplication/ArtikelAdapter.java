package e.wiantoro.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import e.wiantoro.myapplication.model.Artikel;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArtikelAdapter extends RecyclerView.Adapter<ArtikelAdapter.TeamViewholder>{
    private ArrayList<Artikel> artikels;

    private OnClickCallback onClickCallback;

    public OnClickCallback getOnClickCallback() {
        return onClickCallback;
    }

    public void setOnClickCallback(OnClickCallback onClickCallback) {
        this.onClickCallback = onClickCallback;
    }

    public ArrayList<Artikel> getArtikels() {
        return artikels;
    }

    public void setArtikels(ArrayList<Artikel> artikels) {
        this.artikels = artikels;
    }

    @NonNull
    @Override
    public TeamViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_artikel, parent, false);
        return new TeamViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewholder holder, int position) {
        holder.bind(getArtikels().get(position));
    }

    @Override
    public int getItemCount() {
        return getArtikels().size();
    }

    class TeamViewholder extends RecyclerView.ViewHolder{

        ImageView imgGambar;

        TextView tvTitle;

        ImageView imgStar;

        TextView tvDescription;

        TextView tvAuthor;

        public TeamViewholder(View itemView) {
            super(itemView);

            imgGambar = itemView.findViewById(R.id.img_artikel);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            imgStar = itemView.findViewById(R.id.img_star);
        }

        public void bind(Artikel artikel){
            Picasso.get().load(artikel.getUrlToImage())
                    .into(imgGambar);

            tvTitle.setText(artikel.getTitle());
            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getOnClickCallback()
                            .onClicked(getArtikels()
                                    .get(getAdapterPosition()));
                }
            });
            tvDescription.setText(artikel.getDescription());
            tvDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getOnClickCallback()
                            .onClicked(getArtikels()
                                    .get(getAdapterPosition()));
                }
            });
            tvAuthor.setText(artikel.getAuthor());
            tvAuthor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getOnClickCallback()
                            .onClicked(getArtikels()
                                    .get(getAdapterPosition()));
                }
            });

            imgStar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgStar.setImageResource(R.drawable.ic_star_yellow);
                }
            });
        }
    }

    public interface OnClickCallback{
        void onClicked(Artikel artikel);
    }
}

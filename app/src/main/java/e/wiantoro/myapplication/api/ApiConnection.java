package e.wiantoro.myapplication.api;

import e.wiantoro.myapplication.response.ArtikelResponse;

import retrofit2.Call;

public class ApiConnection {
    private static final String BASE_URL =
            "https://newsapi.org/v2/";

    public static Call<ArtikelResponse> getTeamList(String domains,String apiKey){
        return ApiService.createService(ApiClient.class,
                OkHttpClientFactory.create(), BASE_URL)
                .getTeams(domains,apiKey);
    }
}
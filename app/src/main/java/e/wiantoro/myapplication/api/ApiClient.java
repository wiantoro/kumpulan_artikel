package e.wiantoro.myapplication.api;

import e.wiantoro.myapplication.response.ArtikelResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiClient {
    @GET("everything")
    Call<ArtikelResponse> getTeams(@Query("domains") String domains,@Query("apiKey") String apiKey);
}

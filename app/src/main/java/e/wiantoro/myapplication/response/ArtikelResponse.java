package e.wiantoro.myapplication.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import e.wiantoro.myapplication.model.Artikel;

public class ArtikelResponse {

    @SerializedName("articles")
    private ArrayList<Artikel> artikels = new ArrayList<>();

    public ArrayList<Artikel> getArtikels() {
        return artikels;
    }

    public void setArtikels(ArrayList<Artikel> artikels) {
        this.artikels = artikels;
    }
}

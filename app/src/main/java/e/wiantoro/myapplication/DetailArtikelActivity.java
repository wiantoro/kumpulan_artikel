package e.wiantoro.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailArtikelActivity extends AppCompatActivity {

    private TextView tvTitle;

    private TextView tvDescription;

    private TextView tvAuthor;

    private ImageView imgGambar;

    public static final String BUNDLE_TITLE= "bundle_title";
    public static final String BUNDLE_DESCRIPTION= "bundle_description";
    public static final String BUNDLE_AUTHOR= "bundle_author";
    public static final String BUNDLE_GAMBAR = "bundle_gambar";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_artikel);

        tvTitle = findViewById(R.id.tv_title_detail);

        tvAuthor = findViewById(R.id.tv_author_detail);

        tvDescription = findViewById(R.id.tv_description_detail);

        imgGambar = findViewById(R.id.img_artikel_detail);

        String title = getIntent().getStringExtra(BUNDLE_TITLE);

        String gambar = getIntent().getStringExtra(BUNDLE_GAMBAR);

        String description = getIntent().getStringExtra(BUNDLE_DESCRIPTION);

        String author = getIntent().getStringExtra(BUNDLE_AUTHOR);


        Picasso.get().load(gambar).into(imgGambar);

        tvTitle.setText(title);
        tvDescription.setText(description);
        tvAuthor.setText(author);
    }
}
